import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'homepage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final FirebaseAuth auth = FirebaseAuth.instance;

  Future<FirebaseUser> _authUser() async {
    final currentUser = await auth.currentUser();

    print('I found currentUser : ${currentUser.displayName}');

    final queryUser = await Firestore.instance
        .collection('users')
        .where('name', isEqualTo: 'liang liu')
        .getDocuments();

    print('I found ${queryUser.documents.length} user in ${currentUser.displayName}');

    if (queryUser.documents.length == 0) {
      print('----Create New User----');
      Firestore.instance.collection('users').document().setData({
        'id': currentUser.uid,
        'name': currentUser.displayName,
      });
    }
    return currentUser;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'One Book Club',
      home: FutureBuilder<FirebaseUser>(
        future: _authUser(),
        builder: (BuildContext context, AsyncSnapshot<FirebaseUser> snapshot) {
          print('My App User: ${snapshot.data.displayName}');
          return MyHomePage(title: snapshot.data.displayName ?? 'Anonymous');
        },
      ),
      theme: ThemeData(
        primaryColor: Colors.yellow,
      ),
    );
  }
}
