import 'package:flutter/material.dart';

import 'book_search.dart';
import 'bookshelf.dart';
import 'auth.dart';

class MyHomePage extends StatefulWidget {
  final String title;
  MyHomePage({this.title});
  @override
  _MyHomePageState createState() => _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState({Key key, this.title});

  final String title;

  void _userBookList() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('User Books'),
        ),
        body: UserBookList(),
      );
    }));
  }

  void _signin() {
    Navigator.of(context)
        .push((MaterialPageRoute(builder: (BuildContext context) {
      return SigninPageWidget();
    })));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.book), onPressed: _userBookList),
          IconButton(icon: Icon(Icons.person_outline), onPressed: _signin),
        ],
      ),
      body: Container(
        child: SearchBar(),
      ),
    );
  }
}
