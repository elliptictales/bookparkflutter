import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';

import 'book.dart';

class SearchBar extends StatefulWidget {
  @override
  SearchBarState createState() => SearchBarState();
}

class SearchBarState extends State<SearchBar> {
  final _searchController = TextEditingController();

  bool _isISBN(String text) =>
      RegExp(r'\d{10}|\d{13}').hasMatch(text) &&
      (text.length == 10 || text.length == 13);

  List<Book> _books = <Book>[];

  Future<List<Book>> _getDouban(String text) async {
    final books = <Book>[];
    final query = _isISBN(text) ? 'isbn:$text' : text;
    final response =
        await http.get('https://api.douban.com/v2/book/search?q=$query');
    if (response.statusCode == 200) {
      print('Found ${json.decode(response.body)['total']}from Douban!');

      List<dynamic> items = json.decode(response.body)['books'];
      // return when contains nothing
      if (items.length <= 0) return <Book>[];

      for (var item in items) {
        books.add(Book.fromDouban(item));
      }
      return books;
    } else {
      throw Exception('Failed to fetch book infomation');
    }
  }

  Future<List<Book>> _getGoogleBooks(String text) async {
    final books = <Book>[];
    final query = _isISBN(text) ? 'isbn:$text' : text;
    final response =
        await http.get('https://www.googleapis.com/books/v1/volumes?q=$query');
    if (response.statusCode == 200) {
      print(
          'Found ${json.decode(response.body)['totalItems']} from GoogleBooks!');
      List<dynamic> items = json.decode(response.body)['items'];
      // return when contains nothing
      if (items.length < 0) return <Book>[];

      for (var item in items) {
        books.add(Book.fromGoogleBooks(item));
      }
      return books;
    } else {
      throw Exception('Failed to fetch book infomation');
    }
  }

  Widget _buildResaultList(String username) {
    if (_books.isEmpty)
      return Container(
        child: Text('no books infomation'),
      );

    return Flexible(
      child: ListView.builder(
          itemCount: _books.length,
          padding: EdgeInsets.only(top: 10.0),
          //itemExtent: 25.0,
          itemBuilder: (context, i) {
            return _buildTile(_books[i], username);
          }),
    );
  }

  Widget _buildTile(Book book, String username) {
    return ListTile(
      leading: CircleAvatar(
        child: Image.network(book.cover),
      ),
      title: Text(book.title),
      subtitle: Text(book.subtitle),
      trailing: IconButton(
        icon: Icon(Icons.bookmark_border),
        onPressed: () => Firestore.instance
            .collection('bookshelf')
            .document()
            .setData(book.toMap(username)),
      ),
    );
  }

  Widget _buildSearchText() => TextField(
        autofocus: true,
        decoration: InputDecoration(
          labelText: 'Book name or ISBN',
        ),
        //onChanged: (text) {},
        maxLines: 1,
        onSubmitted: (text) {
          _books.clear();
          _getGoogleBooks(text).then((books) {
            setState(() {
              _books.addAll(books);
            });
          });
          _getDouban(text).then((books) {
            setState(() {
              _books.addAll(books);
            });
          });
        },
        controller: _searchController,
      );

  final FirebaseAuth auth = FirebaseAuth.instance;
  Future<FirebaseUser> _authUser() async {
    return await auth.currentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _buildSearchText(),
        FutureBuilder<FirebaseUser>(
          future: _authUser(),
          builder:
              (BuildContext context, AsyncSnapshot<FirebaseUser> snapshot) {
            print('Oooooooooo: ${snapshot.data.displayName}');
            return _buildResaultList(snapshot.data.displayName);
          },
        ),
      ],
    );
  }
}
