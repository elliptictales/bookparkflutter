import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';

class Book {
  String isbn, title, subtitle, language, country, summary, publisher;
  String creator, description, createDate, pubDate;
  String cover;
  List<dynamic> authors, keys, tags, categories, images;
  num pages;

  Book(this.isbn, this.title, this.subtitle, this.cover,
      {this.language,
      this.country,
      this.publisher,
      this.pages,
      this.createDate,
      this.summary,
      this.description,
      this.pubDate,
      this.authors,
      this.keys,
      this.tags,
      this.categories,
      this.images});

  factory Book.fromGoogleBooks(Map<String, dynamic> v) {
    Map<String, dynamic> item = v['volumeInfo'];
    return Book(
      item['industryIdentifiers'][0]['identifier'] ?? '',
      item['title'],
      item['subtitle'] ?? '',
      item['imageLinks']['thumbnail'] ?? '',
      summary: 'summary',
      publisher: item['publisher'] ?? '',
      language: item['language'] ?? '',
      country: item['accessInfo']['country'] ?? '',
      pubDate: item['publishDate'] ?? '',
      pages: item['pageCount'] ?? 0,
    );
  }

  factory Book.fromDouban(Map<String, dynamic> item) {
    return Book(
      item['isbn10'],
      item['title'],
      item['subtitle'] ?? '',
      item['image'] ?? '',
      summary: item['summary'],
      publisher: item['publisher'],
      authors: item['author'],
      pubDate: item['pubdate'],
      pages: int.tryParse(item['pages'] ?? '0'),
    );
  }

  List<dynamic> _keys() {
    final keys = Set();
    if (title != null) keys.addAll(title.split(' '));
    if (subtitle != null) keys.addAll(subtitle.split(' '));
    if (authors != null) keys.addAll(authors);
    return keys.toList();
  }

  Map<String, dynamic> toMap(String creator) {
    return {
      'title': title,
      'subtitle': subtitle,
      'isbn': isbn,
      'pages': pages,
      'language': language,
      'country': country,
      'publisher': publisher,
      'summary': summary,
      'description': description,
      'createDate': DateFormat.yMMMd().format(DateTime.now()),
      'pubDate': pubDate,
      'cover': cover,
      'authors': authors,
      'keys': _keys(),
      'tags': tags,
      'categories': categories,
      'images': images,
      'creator': creator ?? 'who?',
    };
  }
}
