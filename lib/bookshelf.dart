import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserBookList extends StatelessWidget {
  void _removeBook(DocumentSnapshot document) {
    Firestore.instance.runTransaction((t) async {
      t.delete(document.reference);
    });
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    return ListTile(
      key: ValueKey(document.documentID),
      leading: CircleAvatar(
          child: Image.network(
              document['cover'] != null ? document['cover'] : '')),
      title: Text(document['title']),
      subtitle: Text(document['subtitle']),
      trailing: IconButton(
        icon: Icon(Icons.delete),
        onPressed: () => _removeBook(document),
      ),
      /*onTap: () => Firestore.instance.runTransaction((t) async {

            DocumentSnapshot freshSnap = await t.get(document.reference);
            await t
                .update(freshSnap.reference, {'pages': freshSnap['pages'] + 1});
          }),*/
    );
  }

  Widget _buildStream() => StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance
          .collection('bookshelf')
          .where('creator', isEqualTo: 'liang liu')
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return Text('Loading');
        return ListView(
          children: snapshot.data.documents.map((DocumentSnapshot document) {
            return _buildListItem(context, document);
          }).toList(),
        );
      });

  Widget build(BuildContext context) {
    return _buildStream();
  }
}
